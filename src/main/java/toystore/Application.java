package toystore;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import toystore.config.SpringRootConfig;
import toystore.dao.ToyDao;
import toystore.importer.ToyImporter;

public class Application {

    public static void main(String[] args) throws IOException {
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringRootConfig.class);
        ToyDao toyDao = context.getBean(ToyDao.class);
        Resource resource = new ClassPathResource("items.xml");
        InputStream input = resource.getInputStream();
        ToyImporter.importToysToDatabase(toyDao, input);
        Server server = context.getBean(Server.class);
        server.run();
    }


}
