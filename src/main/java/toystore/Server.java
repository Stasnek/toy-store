package toystore;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import toystore.commands.Command;
import toystore.commands.CommandsFactory;
import toystore.commands.SessionContext;


@Component
public class Server {
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    @Autowired
    private CommandsFactory commandsFactory;

    @Autowired
    private Environment env;

    public void run() throws IOException {
        String portProperty = env.getProperty("server.port");
        try (ServerSocket serverSocket = new ServerSocket(Integer.valueOf(portProperty))) {
            System.out.println("Server Started");
            while (true) {
                Socket socket = serverSocket.accept();
                new ClientWorker(socket).start();

            }
        }
    }

    private class ClientWorker extends Thread {
        private final Socket socket;

        private ClientWorker(Socket socket) {
            this.socket = socket;
        }

        public void run() {
            boolean isFirstMessage = true;
            SessionContext context = new SessionContext();
            try (PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                 BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {

                System.out.println("NEW CLIENT! " + socket.toString());
                out.write("WELCOME TO TOY STORE!" + LINE_SEPARATOR);
                out.flush();

                boolean exit = false;
                String arg;
                while (!exit) {
                    String answer = isFirstMessage ? in.readLine().substring(21) : in.readLine();
                    isFirstMessage = false;
                    System.out.println("incoming request: " + answer);
                    String[] splittedCommand = answer.split(" ");

                    Command command = commandsFactory.getCommand(splittedCommand[0]);
                    arg = splittedCommand.length > 1 ? splittedCommand[1] : "";
                    String result = command.execute(arg, context);
                    out.write(result + LINE_SEPARATOR);
                    out.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}