package toystore.commands;

import toystore.model.User;

public class SessionContext {

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
