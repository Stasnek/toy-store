package toystore.commands;

public interface Command {

    String execute(String arg, SessionContext context);
}
