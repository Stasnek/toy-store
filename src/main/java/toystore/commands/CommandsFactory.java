package toystore.commands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import toystore.commands.impl.Buy;
import toystore.commands.impl.Login;
import toystore.commands.impl.Logout;
import toystore.commands.impl.MyInfo;
import toystore.commands.impl.Sell;
import toystore.commands.impl.UnknownCommand;
import toystore.commands.impl.ViewShop;

@Component
public class CommandsFactory {

    @Autowired
    private Login login;

    @Autowired
    private Logout logout;

    @Autowired
    private ViewShop viewShop;

    @Autowired
    private MyInfo myInfo;

    @Autowired
    private Buy buy;

    @Autowired
    private Sell sell;

    public Command getCommand(String command) {
        if ("login".equalsIgnoreCase(command)) {
            return login;
        } else if ("logout".equalsIgnoreCase(command)) {
            return logout;
        } else if ("viewshop".equalsIgnoreCase(command)) {
            return viewShop;
        } else if ("myinfo".equalsIgnoreCase(command)) {
            return myInfo;
        } else if ("buy".equals(command)) {
            return buy;
        } else if ("sell".equals(command)) {
            return sell;
        }

        return new UnknownCommand();
    }
}
