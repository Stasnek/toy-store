package toystore.commands.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import toystore.commands.Command;
import toystore.commands.SessionContext;
import toystore.manager.ToyStoreManager;
import toystore.model.User;

@Component
public class Sell implements Command {

    @Autowired
    private ToyStoreManager manager;


    @Override
    public String execute(String toyName, SessionContext context) {
        User userFromContext = context.getUser();
        if (userFromContext == null) {
            return "You must be logged in.";
        }

        return manager.sellToy(toyName, userFromContext.getLogin());
    }
}
