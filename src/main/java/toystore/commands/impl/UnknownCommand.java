package toystore.commands.impl;

import toystore.commands.Command;
import toystore.commands.SessionContext;

public class UnknownCommand implements Command {

    @Override
    public String execute(String arg, SessionContext context) {
        return "Unknown command!";
    }
}
