package toystore.commands.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import toystore.commands.Command;
import toystore.commands.SessionContext;
import toystore.manager.ToyStoreManager;
import toystore.model.Toy;
import toystore.model.User;

@Component
public class MyInfo implements Command {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    @Autowired
    private ToyStoreManager manager;

    @Override
    public String execute(String arg, SessionContext context) {

        User userFromContext = context.getUser();
        if (userFromContext == null) {
            return "You must be logged in.";
        }

        // force update user from DB
        User user = manager.getUserByLogin(userFromContext.getLogin());
        if (user == null) {
            return "Unable got get info from user " + userFromContext.getLogin();
        }
        context.setUser(user);

        StringBuilder result = new StringBuilder(LINE_SEPARATOR);
        result.append("* login: ")
                .append(user.getLogin())
                .append(LINE_SEPARATOR)
                .append("* money: ")
                .append(user.getMoney())
                .append(LINE_SEPARATOR)
                .append("Toys:")
                .append(LINE_SEPARATOR);

        List<Toy> userToys = manager.getUserToys(user);
        userToys.forEach(toy -> result.append("* ").append(toy).append(LINE_SEPARATOR));

        return result.toString();
    }
}
