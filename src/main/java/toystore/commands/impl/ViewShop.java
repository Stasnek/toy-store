package toystore.commands.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import toystore.commands.Command;
import toystore.commands.SessionContext;
import toystore.manager.ToyStoreManager;
import toystore.model.Toy;

@Component
public class ViewShop implements Command {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    @Autowired
    private ToyStoreManager manager;

    @Override
    public String execute(String arg, SessionContext context) {
        List<Toy> toys = manager.getAvailableToys();
        Map<Toy, Long> groupedToys = toys.stream().collect(
                Collectors.groupingBy(toy -> toy, Collectors.counting()));

        StringBuilder result = new StringBuilder(LINE_SEPARATOR);
        result.append("Toys:")
                .append(LINE_SEPARATOR);

        groupedToys.keySet().forEach(toy ->
                result.append("* ")
                        .append(toy)
                        .append(" amount ")
                        .append(groupedToys.get(toy))
                        .append(LINE_SEPARATOR)
        );

        return result.toString();
    }
}
