package toystore.commands.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import toystore.auth.AuthException;
import toystore.auth.AuthManager;
import toystore.commands.Command;
import toystore.commands.SessionContext;
import toystore.model.User;

@Component
public class Login implements Command {

    @Autowired
    private AuthManager authManager;

    @Override
    public String execute(String login, SessionContext context) {
        try {
            User user = authManager.login(login);
            context.setUser(user);
            return "Logged in as " + user.getLogin();
        } catch (AuthException e) {
            return e.getMessage();
        }
    }
}
