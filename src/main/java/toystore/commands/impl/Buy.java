package toystore.commands.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import toystore.commands.Command;
import toystore.commands.SessionContext;
import toystore.manager.ToyStoreManager;
import toystore.model.User;

@Component
public class Buy implements Command {

    @Autowired
    private ToyStoreManager manager;

    @Override
    public String execute(String toyName, SessionContext context) {

        User user = context.getUser();
        if (user == null) {
            return "You must be logged in.";
        }

        return manager.buyToy(toyName, user.getLogin());
    }
}
