package toystore.commands.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import toystore.auth.AuthManager;
import toystore.commands.Command;
import toystore.commands.SessionContext;
import toystore.model.User;

@Component
public class Logout implements Command {

    @Autowired
    private AuthManager authManager;

    @Override
    public String execute(String arg, SessionContext context) {
        User user = context.getUser();
        if (user == null) {
            return "You are not logged in.";
        }

        authManager.logout(user);
        context.setUser(null);
        return "Logged out.";
    }
}
