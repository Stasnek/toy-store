package toystore.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "items")
public class ToyCatalog {

    @XmlElement(name = "toyInventory")
    private List<ToyInventory> inventoryList;

    @Override
    public String toString() {
        return "ToyCatalog{" +
                "inventoryList=" + inventoryList +
                '}';
    }

    public List<ToyInventory> getInventoryList() {
        return inventoryList;
    }

    public void setInventoryList(List<ToyInventory> inventoryList) {
        this.inventoryList = inventoryList;
    }
}
