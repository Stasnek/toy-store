package toystore.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import toystore.dao.ToyDao;
import toystore.model.Toy;
import toystore.model.User;

@Repository
public class ToyDaoImpl implements ToyDao {

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void add(Toy toy) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("name", toy.getName());
        params.put("description", toy.getDescription());
        params.put("price", toy.getPrice());

        String sql = "INSERT INTO toys (name, description, price) VALUES(:name, :description, :price)";
        namedParameterJdbcTemplate.update(sql, params);
    }

    @Override
    public void delete(Toy toy) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("id", toy.getId());

        String sql = "DELETE FROM toys WHERE id=:id";
        namedParameterJdbcTemplate.update(sql, params);
    }

    @Override
    public List<Toy> getByName(String name) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", name);

        String sql = "SELECT * FROM toys WHERE name=:name";

        return namedParameterJdbcTemplate.query(
                sql,
                params,
                new ToyMapper());
    }

    @Override
    public List<Toy> getUserToys(User user) {
        Map<String, Object> params = new HashMap<>();
        params.put("owner", user.getLogin());

        String sql = "SELECT * FROM toys WHERE owner=:owner";

        return namedParameterJdbcTemplate.query(
                sql,
                params,
                new ToyMapper());
    }

    @Override
    public List<Toy> getUserToysByName(User user, String name) {
        Map<String, Object> params = new HashMap<>();
        params.put("owner", user.getLogin());
        params.put("name", name);

        String sql = "SELECT * FROM toys WHERE owner=:owner and name=:name";

        return namedParameterJdbcTemplate.query(
                sql,
                params,
                new ToyMapper());
    }

    @Override
    public Toy getById(int id) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);

        String sql = "SELECT * FROM toys WHERE id=:id";

        return namedParameterJdbcTemplate.queryForObject(
                sql,
                params,
                new ToyMapper());
    }

    @Override
    public List<Toy> getAll() {
        String sql = "SELECT * FROM toys";
        return jdbcTemplate.query(sql, new ToyMapper());
    }

    @Override
    public List<Toy> getAvailable() {
        String sql = "SELECT * FROM toys WHERE owner IS NULL";
        return jdbcTemplate.query(sql, new ToyMapper());
    }

    @Override
    public List<Toy> getAvailableByName(String toy) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", toy);

        String sql = "SELECT * FROM toys WHERE name=:name AND owner IS NULL";

        return namedParameterJdbcTemplate.query(
                sql,
                params,
                new ToyMapper());
    }

    @Override
    public void saveOrUpdate(Toy toy) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("id", toy.getId());
        params.put("name", toy.getName());
        params.put("description", toy.getDescription());
        params.put("price", toy.getPrice());
        params.put("owner", toy.getOwner());

        String sql = "UPDATE toys SET name = :name, description = :description, price = :price, owner = :owner WHERE id = :id";
        namedParameterJdbcTemplate.update(sql, params);
    }


    private static final class ToyMapper implements RowMapper<Toy> {

        public Toy mapRow(ResultSet rs, int rowNum) throws SQLException {
            Toy toy = new Toy();
            toy.setId(rs.getInt("id"));
            toy.setName(rs.getString("name"));
            toy.setDescription(rs.getString("description"));
            toy.setPrice(rs.getDouble("price"));
            toy.setOwner(rs.getString("owner"));
            return toy;
        }
    }
}
