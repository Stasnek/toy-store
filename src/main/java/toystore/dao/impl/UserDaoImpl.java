package toystore.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import toystore.dao.UserDao;
import toystore.model.User;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void add(User user) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("login", user.getLogin());
        params.put("money", user.getMoney());

        String sql = "INSERT INTO users (login, money) VALUES(:login, :money)";
        namedParameterJdbcTemplate.update(sql, params);
    }

    @Override
    public void delete(User user) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("login", user.getLogin());

        String sql = "DELETE FROM users WHERE login=:login";
        namedParameterJdbcTemplate.update(sql, params);
    }

    @Override
    public User getByLogin(String login) {
        Map<String, Object> params = new HashMap<>();
        params.put("login", login);

        String sql = "SELECT * FROM users WHERE login=:login";

        List<User> result = namedParameterJdbcTemplate.query(
                sql,
                params,
                new UserMapper());

        if (result.isEmpty()) {
            return null;
        }

        return result.get(0);
    }

    @Override
    public List<User> getAll() {
        String sql = "SELECT * FROM users";
        return jdbcTemplate.query(sql, new UserMapper());
    }

    @Override
    public void saveOrUpdate(User user) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("login", user.getLogin());
        params.put("money", user.getMoney());

        String sql = "UPDATE users SET money=:money WHERE login = :login";
        namedParameterJdbcTemplate.update(sql, params);
    }

    private static final class UserMapper implements RowMapper<User> {

        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            user.setLogin(rs.getString("login"));
            user.setMoney(rs.getDouble("money"));
            return user;
        }
    }
}
