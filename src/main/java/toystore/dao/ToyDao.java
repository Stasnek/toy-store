package toystore.dao;

import java.util.List;

import toystore.model.Toy;
import toystore.model.User;

public interface ToyDao {

    void add(Toy toy);

    void delete(Toy toy);

    List<Toy> getByName(String toy);

    Toy getById(int toy);

    List<Toy> getAll();

    List<Toy> getAvailable();

    List<Toy> getAvailableByName(String toy);

    List<Toy> getUserToys(User user);

    List<Toy> getUserToysByName(User user, String name);

    void saveOrUpdate(Toy toy);
}
