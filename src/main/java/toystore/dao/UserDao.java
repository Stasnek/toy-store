package toystore.dao;

import java.util.List;

import toystore.model.User;

public interface UserDao {

    void add(User user);

    void delete(User user);

    User getByLogin(String login);

    List<User> getAll();

    void saveOrUpdate(User user);
}
