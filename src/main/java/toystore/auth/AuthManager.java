package toystore.auth;

import toystore.model.User;

public interface AuthManager {

    User login(String login) throws AuthException;

    void logout(User user);

}