package toystore.auth.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import toystore.auth.AuthException;
import toystore.auth.AuthManager;
import toystore.dao.UserDao;
import toystore.model.User;

@Component
public class AuthManagerImpl implements AuthManager {

    private final Object lock = new Object();
    @Autowired
    private UserDao userDao;
    private Set<User> activeUsers = new HashSet<>();

    public User login(String login) throws AuthException {
        synchronized (lock) {
            User user = userDao.getByLogin(login);
            if (user == null) {
                throw new AuthException("Auth failed. User " + login + " not found.");
            }

            if (activeUsers.contains(user)) {
                throw new AuthException("Auth failed. User " + login + " has active session.");
            }
            activeUsers.add(user);
            return user;
        }
    }

    public void logout(User user) {
        synchronized (lock) {
            activeUsers.remove(user);
        }
    }
}
