package toystore.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import toystore.dao.ToyDao;
import toystore.dao.UserDao;
import toystore.manager.ToyStoreManager;
import toystore.model.Toy;
import toystore.model.User;

import java.util.List;

@Component
public class ToyStoreManagerImpl implements ToyStoreManager {

    @Autowired
    private ToyDao toyDao;

    @Autowired
    private UserDao userDao;

    @Override
    public User getUserByLogin(String login) {
        return userDao.getByLogin(login);
    }

    @Override
    public List<Toy> getUserToys(User user) {
        return toyDao.getUserToys(user);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public String buyToy(String toyName, String login) {
        User user = userDao.getByLogin(login);
        if (user == null) {
            return "Unable got get info from user " + login;
        }

        List<Toy> toys = toyDao.getAvailableByName(toyName);
        if (toys.isEmpty()) {
            return "Toy \"" + toyName + "\" not found.";
        }

        Toy toy = toys.get(0);
        Double toyPrice = toy.getPrice();
        Double userMoney = user.getMoney();
        if (toyPrice > userMoney) {
            return "You don't have enough money to buy \"" + toyName + "\".";
        }

        user.setMoney(userMoney - toyPrice);
        userDao.saveOrUpdate(user);

        toy.setOwner(user.getLogin());
        toyDao.saveOrUpdate(toy);

        return "Purchase completed. \"" + toyName + "\" price " + toyPrice;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public String sellToy(String toyName, String login) {
        User user = userDao.getByLogin(login);
        if (user == null) {
            return "Unable got get info from user " + login;
        }

        List<Toy> toys = toyDao.getUserToysByName(user, toyName);
        if (toys.isEmpty()) {
            return "Toy \"" + toyName + "\" not found.";
        }

        Toy toy = toys.get(0);
        Double toyPrice = toy.getPrice();
        Double userMoney = user.getMoney();

        user.setMoney(userMoney + toyPrice);
        userDao.saveOrUpdate(user);

        toy.setOwner(null);
        toyDao.saveOrUpdate(toy);

        return "Purchase completed. \"" + toyName + "\" price " + toyPrice;
    }

    @Override
    public List<Toy> getAvailableToys() {
        return toyDao.getAvailable();
    }
}
