package toystore.manager;

import toystore.model.Toy;
import toystore.model.User;

import java.util.List;

public interface ToyStoreManager {
    User getUserByLogin(String login);

    List<Toy> getUserToys(User user);

    String buyToy(String toyName, String login);

    String sellToy(String toyName, String login);

    List<Toy> getAvailableToys();
}
