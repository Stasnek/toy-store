package toystore.importer;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import toystore.dao.ToyDao;
import toystore.model.ToyCatalog;
import toystore.model.ToyInventory;

public final class ToyImporter {

    private ToyImporter() {
    }

    public static void importToysToDatabase(ToyDao toyDao, InputStream inputStream) {
        ToyCatalog catalog = ToyImporter.unmarshal(inputStream);
        List<ToyInventory> inventoryList = catalog.getInventoryList();
        for (ToyInventory toyInventory : inventoryList) {
            for (int i = 0; i < toyInventory.getAmount(); i++) {
                toyDao.add(toyInventory.getToy());
            }
        }
    }

    private static ToyCatalog unmarshal(InputStream inputStream) {
        try (InputStream is = new BufferedInputStream(inputStream)) {
            JAXBContext jc = JAXBContext.newInstance(ToyCatalog.class);
            Unmarshaller um = jc.createUnmarshaller();
            return (ToyCatalog) um.unmarshal(is);
        } catch (JAXBException | IOException e) {
            e.printStackTrace();
        }
        return new ToyCatalog();
    }
}
