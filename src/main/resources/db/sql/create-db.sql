create TABLE IF NOT EXISTS users (
  login VARCHAR(30) PRIMARY KEY,
  money DOUBLE
);

create TABLE IF NOT EXISTS toys (
  id bigint auto_increment PRIMARY KEY,
  name VARCHAR(30),
  description VARCHAR(200),
  price DOUBLE,
  owner VARCHAR(30) NULL
);

alter table toys add CONSTRAINT IF NOT EXISTS toys_users_fk FOREIGN KEY (owner) REFERENCES users (login) ON delete CASCADE;
